﻿namespace CollectiveWork
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.THEBESTBUTTONEVER = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добавьте свою кнопку сюда";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(196, 117);
            this.button1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 44);
            this.button1.TabIndex = 1;
            this.button1.Text = "My Button";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // THEBESTBUTTONEVER
            // 
            this.THEBESTBUTTONEVER.Location = new System.Drawing.Point(684, 6);
            this.THEBESTBUTTONEVER.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.THEBESTBUTTONEVER.Name = "THEBESTBUTTONEVER";
            this.THEBESTBUTTONEVER.Size = new System.Drawing.Size(386, 90);
            this.THEBESTBUTTONEVER.TabIndex = 2;
            this.THEBESTBUTTONEVER.Text = "THE BEST BUTTON EVER created by Azamat Mullagaliev";
            this.THEBESTBUTTONEVER.UseVisualStyleBackColor = true;
            this.THEBESTBUTTONEVER.Click += new System.EventHandler(this.THEBESTBUTTONEVER_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 558);
            this.Controls.Add(this.THEBESTBUTTONEVER);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button THEBESTBUTTONEVER;
    }
}

